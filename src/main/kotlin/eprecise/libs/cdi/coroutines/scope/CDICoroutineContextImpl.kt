package eprecise.libs.cdi.coroutines.scope

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asContextElement
import kotlinx.coroutines.cancel
import mu.KLogging
import java.util.logging.Logger
import javax.enterprise.context.ContextNotActiveException
import javax.enterprise.context.spi.Contextual
import javax.enterprise.context.spi.CreationalContext
import javax.enterprise.inject.Vetoed
import javax.enterprise.inject.spi.BeanManager

@Vetoed
internal class CDICoroutineContextImpl : CDICoroutineContext {

    private val currentContext get() = StoreContainer.store

    override val scope get() = this.currentContext.get()?.scope ?: throw ContextNotActiveException()

    override fun getScope() = CoroutineScoped::class.java

    override fun asContextElement() = currentContext.asContextElement()

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(contextual: Contextual<T>, creationalContext: CreationalContext<T>?): T? {
        synchronized(currentContext) {
            val ctx = currentContext.get()?.map ?: throw ContextNotActiveException()
            var instance: ContextualInstance<T>? = ctx[contextual] as ContextualInstance<T>?
            if (instance == null && creationalContext != null) {
                instance = ContextualInstance(contextual.create(creationalContext), creationalContext, contextual)
                ctx[contextual] = instance
            }
            return instance?.get()
        }
    }

    override fun <T> get(contextual: Contextual<T>) = get(contextual, null)

    override fun isActive() = currentContext.get() != null

    override fun destroy(contextual: Contextual<*>) {
        synchronized(currentContext) {
            val ctx = currentContext.get()?.map ?: return
            ctx.remove(contextual)?.destroy()
        }
    }

    override fun activate(scope: CoroutineScope) {
        synchronized(currentContext) {
            currentContext.set(StoreImpl(scope))
        }
    }

    override fun deactivate() {
        synchronized(currentContext) {
            val ctx = currentContext.get()?.map ?: return
            for (instance in ctx.values) {
                try {
                    instance.destroy()
                } catch (e: Exception) {
                    logger.warn { "Unable to destroy instance ${instance.get()} for bean: ${instance.contextual}" }
                }

            }
            ctx.clear()
            scope.cancel()
            currentContext.remove()
        }
    }

    internal class InjectableCDICoroutineContext(
            private val delegate: CDICoroutineContext,
            private val beanManager: BeanManager
    ) : CDICoroutineContext by delegate {

        private var isActivator: Boolean = false

        override fun activate(scope: CoroutineScope) {
            try {
                beanManager.getContext(CoroutineScoped::class.java)
                logger.warn { "CDICoroutineContext context already active" }
            } catch (e: ContextNotActiveException) {
                // Only activate the context if not already active
                delegate.activate(scope)
                isActivator = true
            }

        }

        override fun deactivate() {
            if (isActivator) {
                delegate.deactivate()
            } else {
                logger.warn { "CDICoroutineContext not activated by this bean" }
            }
        }

    }

    internal class ContextualInstance<T>(
            private val value: T,
            private val creationalContext: CreationalContext<T>,
            val contextual: Contextual<T>
    ) {

        fun get() = value

        fun destroy() {
            contextual.destroy(value, creationalContext)
        }

    }

    internal data class StoreImpl(
            val scope: CoroutineScope,
            val map: MutableMap<Contextual<*>, ContextualInstance<*>> = mutableMapOf()
    ) : CDICoroutineContext.Store

    internal object StoreContainer {
        val store = ThreadLocal<StoreImpl?>()
    }

    companion object : KLogging()
}
