package eprecise.libs.cdi.coroutines.scope

import org.slf4j.LoggerFactory
import javax.enterprise.context.Dependent
import javax.enterprise.inject.Default
import javax.enterprise.inject.spi.AfterBeanDiscovery
import javax.enterprise.inject.spi.BeanManager

class CDICoroutineContextExtension {

    fun configure(event: AfterBeanDiscovery, beanManagerSupplier: () -> BeanManager) {
        logger.debug("Registrando CDICoroutinesContext")
        val commandContext = CDICoroutineContextImpl()

        event.addContext(commandContext)

        event.addBean<Any>()
                .addType(CDICoroutineContext::class.java)
                .createWith<Any> {
                    CDICoroutineContextImpl.InjectableCDICoroutineContext(commandContext, beanManagerSupplier())
                }.addQualifier(Default.Literal.INSTANCE)
                .scope(Dependent::class.java)
                .beanClass(CDICoroutineContextExtension::class.java)

    }

    companion object {
        private val logger = LoggerFactory.getLogger(CDICoroutineContextExtension::class.qualifiedName)
    }

}
