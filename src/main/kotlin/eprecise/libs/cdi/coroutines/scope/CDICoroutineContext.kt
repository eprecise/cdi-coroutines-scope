package eprecise.libs.cdi.coroutines.scope

import kotlinx.coroutines.*
import kotlinx.coroutines.future.future
import javax.enterprise.context.spi.AlterableContext


interface CDICoroutineContext : AlterableContext {

    val scope: CoroutineScope

    fun asContextElement(): ThreadContextElement<*>

    fun activate(scope: CoroutineScope)

    fun deactivate()

    fun newContext(dispatcher: CoroutineDispatcher, action: suspend CoroutineScope.() -> Unit) = CoroutineScope(dispatcher).launch {
        try {
            activate(this)
            run(action)
        } finally {
            deactivate()
        }
    }

    fun future(action: suspend CoroutineScope.() -> Unit) = scope.future(asContextElement()) { action() }

    fun launch(action: suspend CoroutineScope.() -> Unit) = scope.launch(asContextElement()) { action() }

    suspend fun run(action: suspend CoroutineScope.() -> Unit) = launch(action).join()

    fun runBlocking(action: suspend CoroutineScope.() -> Unit) = runBlocking<Unit> { run(action) }

    interface Store

    companion object {
        val instance: CDICoroutineContext get() = CDICoroutineContextImpl()

        fun future(action: suspend CoroutineScope.() -> Unit) = instance.future(action)

        fun launch(action: suspend CoroutineScope.() -> Unit) = instance.launch(action)

        fun runBlocking(action: suspend CoroutineScope.() -> Unit) = instance.runBlocking(action)

        suspend fun run(action: suspend CoroutineScope.() -> Unit) = instance.run(action)
    }

}
